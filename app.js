require('dotenv').config()

const express = require('express')
const cors = require('cors')
const mongoose = require('mongoose')

const app = express()

app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(cors())

const routes = {
    classroom: require('./routes/classroom'),
    instructor: require('./routes/instructor'),
    student: require('./routes/student'),
    submission: require('./routes/submission'),
    task: require('./routes/task')
}

app.use('/api/classrooms', routes.classroom)
app.use('/api/instructors', routes.instructor)
app.use('/api/students', routes.student)
app.use('/api/submissions', routes.submission)
app.use('/api/tasks', routes.task)

mongoose.connect("mongodb+srv://jhnnycyn:dxtr008X@cluster0.z5eip.mongodb.net/track?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

const db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("We're connected to the database"))

app.listen(process.env.PORT, () => console.log(`Server is now running at http//localhost:${process.env.PORT}`))