const mongoose = require('mongoose')
const { Schema } = mongoose
const { ObjectId } = mongoose.Schema.Types

const schema = new Schema({
    firstName: {
        type: String,
        required: [true, "First name is required"]
    },
    middleName: String,
    lastName: {
        type: String,
        required: [true, "Last name is required"]
    },
    username: {
        type: String,
        required: [true, "Username is required"],
    },
    email: {
        type: String,
        required: [true, "Email is required"],
        lowercase: true
    },
    password: {
        type: String,
        required: [true, "Password is required"]
    },
    isActive: {
        type: Boolean,
        default: true
    },
    submission: [
        {
            submittedOn: Date,
            grade: Number,
            status: {
                type: String,
                default: "No Grade"
            },
            task: {
                type: ObjectId,
                ref: 'Task'
            }
        }
    ],
    classroom: [
        {
            type: ObjectId,
            ref: 'Classroom'
        }
    ]
})

module.exports = mongoose.model('Student', schema)