const mongoose = require('mongoose')
const { Schema } = mongoose
const { ObjectId } = mongoose.Schema.Types

const schema = new Schema({
    submittedOn: Date,
    grade: Number,
    status: {
        type: String,
        default: "No Grade"
    },
    student: {
        type: ObjectId,
        ref: 'Student'
    },
    task: {
        type: ObjectId,
        ref: 'Task'
    }
})

module.exports = mongoose.model('Submission', schema)