const mongoose = require('mongoose')
const { Schema } = mongoose

const schema = new Schema({
    firstName: {
        type: String,
        required: [true, "First name is required"]
    },
    middleName: String,
    lastName: {
        type: String,
        required: [true, "Last name is required"]
    },
    username: {
        type: String,
        required: [true, "Username is required"],
    },
    email: {
        type: String,
        required: [true, "Email is required"],
        lowercase: true
    },
    password: {
        type: String,
        required: [true, "Password is required"]
    },
    isActive: {
        type: Boolean,
        default: true
    },
})

module.exports = mongoose.model('Instructor', schema)