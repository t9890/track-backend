const mongoose = require('mongoose')
const { Schema } = mongoose
const { ObjectId } = mongoose.Schema.Types

const schema = new Schema({
    name: {
        type: String,
        required: [true, "Name of task is required"]
    },
    type: {
        type: String,
        required: [true, "Type of task is required"]
    },
    createdOn: {
        type: Date,
        default: new Date()
    },
    deadline: {
        type: Date,
        required: [true, "Deadline of a task is required"]
    },
    isActive: {
        type: Boolean, 
        default: true 
    },
    instructor: {
        type: ObjectId,
        ref: 'Instructor'
    },
    classroom: {
        type: ObjectId,
        ref: 'Classroom'
    }
})

module.exports = mongoose.model('Task', schema)