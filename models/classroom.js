const mongoose = require('mongoose')
const { Schema } = mongoose
const { ObjectId } = mongoose.Schema.Types

const schema = new Schema({
    name: {
        type: String,
        required: [true, "Name of classroom is required"]
    },
    subject: {
        type: String,
        required: [true, "Name of subject is required"]
    },
    createdOn: {
        type: Date,
        default: new Date()
    },
    isActive: {
        type: Boolean,
        default: true,
    },
    instructor: {
        type: ObjectId,
        ref: 'Instructor'
    },
    student: [
        {
            type: ObjectId,
            ref: 'Student'
        }
    ]
})

module.exports = mongoose.model('Classroom', schema)